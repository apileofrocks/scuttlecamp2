#!/usr/bin/env bash
echo "~~* Hi! Let's publish the site!"
raco pollen render -s src
raco pollen render -s src/guide
echo "~~* rendered pollen markup to sweet html"
rm -rf www/*
rsync -avm --include='*.html' -f 'hide,! */' src/ www
echo "~~* html files copied from src to www"
echo "~~* Friend, you got yrself a site!  check it out in www and have a good day! *~~"
