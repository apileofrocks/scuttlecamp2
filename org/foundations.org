#+NAME: Foundations of the Scuttlecamp Site

* Purpose
  Lay out the key points and directions for the scuttlecamp site, the foundations of the site.
* Resources
  - [[https://hackmd.io/hlziIAeJQkmXaA1rRQwoqA?view][HACKMD with content from Glyph]]
  - [[https://github.com/ssbc/two.camp.scuttlebutt.nz][Original scuttclecamp site]]
  - Coordination Calls
    - [[https://hackmd.io/85WCZ8ndR0ezb2gcIUrCbQ][First Call]]
    - [[https://hackmd.io/PHs6GvvwTw-jgO5hJ-f_zQ][Second Call]]
    - [[https://hackmd.io/a8ybfeyvTZ-o-fTqUcohdw][Third Call]]
  - Important Plants
    - [[https://en.wikipedia.org/wiki/Jabuticaba][Jabuticaba]]
    - [[https://en.wikipedia.org/wiki/Hymenaea_courbaril][courbaril]]
    - [[https://en.wikipedia.org/wiki/Morus_(plant)][Morus]]
    - [[https://www.youtube.com/watch?v=pojy7FY9Uks][Mangaba (video)]]
* Imporant things      
  - The form will currently forward info to Glyph - gnomad@cryptolab.net - who will coordinate sending out invoices to participants
* Probable parts of the site
  - Home splash (has the poster image)
  - Register
* Questionnaire
  
  #+NAME: Questionnaire
  #+begin_src markdown
    # Scuttlecamp 2: Registration Form

    Scuttlecamp will take place 8 - 12 June 2020, in the village of Moinho, near Alto Paraiso in Goiás, Brazil. Tickets are $200 USD and include meals and accommodation.

    Please fill out this form separately for each adult attending.

    ## About You

    - Name
    - Email
    - SSB ID
    - Emergency Contact
    - Who should we call (name and number)? If they know you by another name, please tell us (we will not share it with anyone)

    ## Costs & Payment

    - Do you need a ticket subsidy?
    - We have support for those experiencing financial hardship or who are already maxing their budget to pay for transport. Subsidized tickets are half price, $125, and are limited in number. If you request a subsidy, we will get in touch with you to confirm.
    - No thanks, I'm all good
    - Yes, please consider me for a subsidized ticket

    ## Logistics

    - Transportation to the venue
    - We're helping connect people for travel from Brasilia International Airport to Moinho (about 3 hours by public transport)
    - I will travel alone
    - I'd like to travel with other Butts
    - Dietary requirements
    - Vegetarian meals are provided as part of your ticket. Tick all further requirements:
    - Vegan
    - Gluten-free
    - Nut allergy
    - Other:
    - Are you bringing children?

    ## Your Scuttlecamp

    - Your vision
    - What is your idea of a great Scuttlecamp experience? What do you most want from this?
    - Your contribution
    - This will be a co-created experience. Is there anything you would like to offer? It could be hosting a conversation, setting up breakfast, sharing music, offering a workshop, leading a hike, or anything else. This isn't a commitment — it's just to get everyone thinking.
    - Do you speak Portugese?
    - Would you like to join a class to learn?
    - Will you be attending the pre-event build week?
    - Do you plan to stay for the post-event informal adventures?
    - Any comments or questions?
    - Scuttlecamp Code of Conduct
    - I have read, understood, and agree to the code of conduct
  #+end_src
  
