#lang pollen

◊define-meta[title]{About}

Scuttlebutt Camp is a gathering of the butts in Brazil, that starts in early June 2020 and, potentially, has no end.

The gathering will be held in Moinho, a small village of approximately 300 residents in the Alto Paraíso municipality of Goiás, Brazil.

◊photo{entrance}

◊h2{Dates}

There are three stages of the camp:

◊fancy-list{
◊strong{1-7 June 2020}: Pre-Event Build


◊strong{8-12 June 2020}: The Main Event.


◊strong{13 June 2020 onward}: The post event hangout.
}

	The core of the event will be from the 8th to the 12th, but butts are invited to join in the pre- or post- hangs as fancy suits them.

◊h2{Contact}

Please don’t hesitate to contact glyph if you have any queries regarding the event.  You can also reach out to Luandro.

◊contact[
#:name "Glyph"
#:email "gnomad@cryptolab.net"
#:socials '(("ssb id" "@HEqy940T6uB+T+d9Jaa58aNfRzLx9eRWqkZljBmnkmk=.ed25519")
("mastodon id" "https://merveilles.town/@glyph"))
]{Glyph is helping organize Scuttlecamp2, June 2020 in Brasilia.}

◊contact[
#:name "Luandro"
#:email "luandro@gmail.com"
]{Luandro is helping organize Scuttlecamp2, June 2020 in Brasilia.}

◊h2{On Scuttlebutt}

You can join the conversation and excitement in the Scuttleverse by joining the channels ◊abbr[#:title "type this hash into your scuttlebutt client of choice to find and follow this channel."]{#scuttlecamp} and ◊abbr[#:title "type this hash into your scuttlebutt client of choice to find and follow this channel."]{"#scuttlecamp"#scuttlecamp2}.


◊sticker{guardian}
◊h2{More Information}

We have more information -- code of conduct, travel prep tips, language guides, and more -- available in our ◊a[#:href "/guide"]{guide}.

You can ◊a[#:href "register.html"]{register for the camp now.}
