#lang pollen

◊(define-meta template "guide-template.html")
◊define-meta[title]{Contact Info}

Please don’t hesitate to contact glyph if you have any queries regarding the event.  You can also reach out to Luandro.

◊contact[
#:name "Glyph"
#:email "gnomad@cryptolab.net"
#:socials '(("ssb id" "@HEqy940T6uB+T+d9Jaa58aNfRzLx9eRWqkZljBmnkmk=.ed25519")
            ("mastodon id" "https://merveilles.town/@glyph"))
]{Glyph is helping organize Scuttlecamp2, June 2020 in Brasilia.}

◊contact[
#:name "Luandro"
#:email "luandro@gmail.com"
]{Luandro is helping organize Scuttlecamp2, June 2020 in Brasilia.}

◊sticker{toad1}
