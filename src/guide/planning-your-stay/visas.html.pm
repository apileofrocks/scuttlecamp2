#lang pollen
◊(define-meta template "../guide-template.html")
◊define-meta[title]{Visas}

Visa exemptions exist for passport holders of many different countries, allowing visits to Brazil of up to 90 days without a visa. We recommend you contact your local Brazilian embassy or consulate to ask for accurate information concerning visa requirements.

Contact details for Brazilian embassies can be found on the ◊a[#:href "https://embassy.goabroad.com/embassies-of/brazil"]{GoAbroad website}. General visa information can be found on the following Wikipedia page: ◊a[#:href "https://en.wikipedia.org/wiki/Visa_policy_of_Brazil"]{Visa policy of Brazil}.
