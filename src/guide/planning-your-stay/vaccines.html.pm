#lang pollen
◊(define-meta template "../guide-template.html")
◊define-meta[title]{Vaccines}

We recommend contacting your local doctor or travel clinic to ensure you have the correct vaccinations for travel to Brazil. In addition to routine vaccinations, further vaccinations may also be recommended by your doctor or required by the government of your home country (for example, hepatitis A, typhoid and yellow fever).

The Centers for Disease Control and Prevention of the USA has a page titled ◊a[#:href "https://wwwnc.cdc.gov/travel/destinations/traveler/none/brazil"]{Health Information for Travelers to Brazil} which provides a useful overview of the situation.

◊sticker{toad2}
