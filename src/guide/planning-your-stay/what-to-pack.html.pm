#lang pollen
◊(define-meta template "../guide-template.html")
◊define-meta[title]{What To Pack}

Scuttlecamp 2 is taking place in the dry, winter month of June. The average temperature for Moinho in June is 20.1°C (68.2 F), with a maximum of 25.9°C (78.6 F) and a minimum of 14.3°C (57.7 F). June represents the start of the dry season in the region and averages just 12mm precipitation.

◊h2{Packing Suggestions}

◊sticker[#:class "inset right"]{traveller1}
◊fancy-list{

Biodegradable soap and toothpaste (without synthetic preservatives and detergents)


For example, ◊a[#:href "https://shop.drbronner.com"]{Dr. Bronner's pure-castile soap}


Swimsuit and towel for swimming


Sleeping bag and pillow


Mosquito net


Musical instruments


Games and sports gear


Fun crafts or activities to share


Hiking / walking shoes


Natural insect repellant


Torch / headlamp / solar-powered lights


Hat / cap
}
