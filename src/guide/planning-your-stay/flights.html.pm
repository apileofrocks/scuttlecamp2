#lang pollen
◊(define-meta template "../guide-template.html")
◊define-meta[title]{Flights}

The International Airport of Brasilia (BSB) is the closest major airport to the Scuttlecamp location in Moinho. We recommend attendees fly to this location before continuing the journey by motor vehicle.
