#lang pollen
◊(define-meta template "../guide-template.html")
◊define-meta[title]{Learning Portuguese}

We are hosting online, communal language-learning sessions in the months leading up to Scuttlecamp 2. All Scuttlecamp participants are warmly invited to join in the fun. Session details will be posted on Scuttlebutt and will also be included in the ◊a[#:href "/news.html"]{News} section of the Scuttlecamp 2 website.

◊photo{bartolemeu}
