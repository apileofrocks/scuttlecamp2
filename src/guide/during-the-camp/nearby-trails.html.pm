#lang pollen

◊(define-meta template "../guide-template.html")
◊define-meta[title]{Nearby Trails}

The most popular attraction here in Moinho is the Solarion complex, that has three waterfalls and various river spots. From the venue to the first waterfall it’s a small walk, about 1 km. From there to the largest waterfalls it’s a beaultiful 2.5 km trail in the woods.

There are many other trails around, none known much to people beyond the village.

◊photo{arcanjos}
