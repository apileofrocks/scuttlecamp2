#lang pollen

◊(define-meta template "../guide-template.html")
◊define-meta[title]{Toilets}

In addition to the existing on-site toilets, we will be constructing compost toilets in the village center during the pre-event build week. Design and organisation of the build will be taking place on Scuttlebutt in the run-up to the event.
