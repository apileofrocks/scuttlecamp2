#lang pollen

◊(define-meta template "../guide-template.html")
◊define-meta[title]{Accomodation}

◊h2{Accomodation Options}
There are three accomodation options for Scuttlecamp 2 participants: camping, house-share and home-stay.

Camping will take place on green space with fruit trees at the center of the village. Campers are expected to bring their own tents, sleeping pads and sleeping bags. If you wish to camp but do not have your own gear, please reach out to the organisers and we will connect you with Butts who may have spare tent-space or gear to share.

There is a large house in the village center, along with two houses nearby, which have beds and will be available as accomodation for Scuttlecamp participants. This represents the house-share option.

The final accomodation option is home-stay. This is an opportunity for participants to be paired with a local family for their sleeping arrangements and breakfast. The organisers of Scuttlecamp will communicate closely with home-stay participants to ensure the pairing represents a good fit for all concerned. This is an awesome opportunity to practice Portugese and connect with Moinho locals on a more intimate level.