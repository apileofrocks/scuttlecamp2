#lang pollen

◊(define-meta template "../guide-template.html")
◊define-meta[title]{Food}

Breakfast, lunch, dinner and snacks are all included in the ticket cost for the event. Food will be vegetarian, with a mixture of catered and communally-prepared meals. If you have other dietary needs (vegan, gluten-free etc.) please indicate so on the registration form and we will ensure you are catered for.

Meal preparation and cooking will take place at the church kitchen close to the village center.
