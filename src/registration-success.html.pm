#lang pollen

◊define-meta[title]{Success!}

Heck yeah, you successfully registered!

We'll reach out to you shortly.

In the meantime, check out our ◊a{guide}.
